package net.crunchdroid.android.recipes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    public void goOtherActivity(View view) {
        Intent intent = new Intent("net.crunchdroid.android.recipes.OtherActivity");
        startActivity(intent);
    }

    public void goOtherOneActivity(View view) {
        Intent intent = new Intent(this, OtherOneActivity.class);
        startActivity(intent);
    }

    public void goNonexistentActivity(View view) {
        Intent intent = new Intent("net.crunchdroid.android.recipes.NonexistentActivity");
        startActivity(Intent.createChooser(intent, "Choose application"));
    }
}
